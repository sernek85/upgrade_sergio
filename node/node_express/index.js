

const express = require('express');

require('./db.js')

const PORT = 3000;
const server = express();



const router = express.Router()

router.get('/',(req,res)=>{
   
    res.send('hola sergio salao')
})

router.get('/familia/:name',(req,res)=>{
    const nameFamily = req.params.name
    const familia = ['sergio','elsa','lia','luna']
    
    const findfamilyIndex = familia.indexOf(nameFamily)
    if(findfamilyIndex === -1){
        res.send('ese nombre no existe')
    }
    res.send(familia[findfamilyIndex])
})

server.use('/', router);

server.listen(PORT, () =>{
    console.log(`servidor arrancado en http://localhost:${PORT}`);
})
