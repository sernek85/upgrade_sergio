"use strict";

var mongoose = require('mongoose');

var Character = require('../models/Character');

var characters = [{
  name: 'Ursula Corberó',
  age: 32,
  alias: 'Tokio'
}, {
  name: 'Pedro Alonso',
  age: 50,
  alias: 'Berlín'
}, {
  name: 'Álvaro Morte',
  age: 46,
  alias: 'Profesor'
}, {
  name: 'Alba Flores',
  age: 34,
  alias: 'Nairobi'
}, {
  name: 'Jaime Lorente',
  age: 29,
  alias: 'Denver'
}, {
  name: 'Darko Peric',
  age: 44,
  alias: 'Helsinki'
}];
var characterDocuments = characters.map(function (character) {
  return new Character(character);
});
mongoose.connect('mongodb://localhost:27017/casa-de-papel', {
  useNewUrlParser: true,
  useUnifiedTopology: true
}).then(function _callee() {
  var allCharacter;
  return regeneratorRuntime.async(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return regeneratorRuntime.awrap(Character.find());

        case 2:
          allCharacter = _context.sent;

          if (!allCharacter.length) {
            _context.next = 6;
            break;
          }

          _context.next = 6;
          return regeneratorRuntime.awrap(Character.collection.drop());

        case 6:
        case "end":
          return _context.stop();
      }
    }
  });
})["catch"](function (err) {
  return console.log("Error deleting data: ".concat(err));
}).then(function _callee2() {
  return regeneratorRuntime.async(function _callee2$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.next = 2;
          return regeneratorRuntime.awrap(Character.insertMany(characterDocuments));

        case 2:
        case "end":
          return _context2.stop();
      }
    }
  });
})["catch"](function (err) {
  return console.log("Error deleting data: ".concat(err));
})["finally"](function () {
  return mongoose.disconect();
}); // nos desconectamos de la bbdd