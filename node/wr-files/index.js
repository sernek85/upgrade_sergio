

const fs = require('fs')

const movies = [{
        name: "Your Name",
        durationInMinutes: 130
    },
    {
        name: "Pesadilla antes de navidad",
        durationInMinutes: 225
    },
    {
        name: "Origen",
        durationInMinutes: 165
    },
    {
        name: "El señor de los anillos",
        durationInMinutes: 967
    },
    {
        name: "Solo en casa",
        durationInMinutes: 214
    },
    {
        name: "El jardin de las palabras",
        durationInMinutes: 40
    }
]


fs.writeFile('movies.json',JSON.stringify(movies) ,()=>{
    console.log('archivo creado');
})

fs.readFile('movies.json', (err, data) => {
    if (err) throw err;
    const parsemovies = JSON.parse(data)
    console.log('lectura correcta' , parsemovies);
  });

  