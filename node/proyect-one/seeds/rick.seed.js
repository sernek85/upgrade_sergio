
const axios = require('axios');
const mongoose = require('mongoose');
const Rick = require('../models/Rick');
const { DB_URL, CONFIG_DB } = require('../utils/db');

const getRickData = async () => {
    const response = await axios('https://rickandmortyapi.com/api/character/?page=1');

    const ricks = response.data.results;
    console.log(ricks);

    runSeed(ricks);
};

getRickData();



const runSeed = (personajesList) => {
    mongoose.connect(DB_URL, CONFIG_DB)
        .then(async () => {
            console.log('Ejecutando seed de rick...');
           
            const allPersonajes = await Rick.find();
           
            if (allPersonajes.length) {
                await Rick.collection.drop();
                console.log('Colección  eliminada con éxito');
            }
        })
        .catch(error => console.log('Error buscando en la DB', error))
        .then(async () => {
            
            await Rick.insertMany(personajesList);
            console.log('Añadidos nuevos personajes a DB');
        })
        .catch(error => console.log('Error añadiendo los nuevos personajes', error))
        
        .finally(() => mongoose.disconnect());
}




