

const { urlencoded } = require('express');
const mongoose = require('mongoose')

const Schema = mongoose.Schema;

const rickSchema = new Schema(
  {
    name: { type: String, required: true },
    status:{type: String},
    species: { type: String },
    image:{type:String}
  },
  {
    timestamps: true,
  }
);

const Rick = mongoose.model('ricks', rickSchema);
module.exports = Rick;

