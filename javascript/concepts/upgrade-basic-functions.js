// Iteración #1: Buscar el máximo
// Completa la función que tomando dos números como argumento devuelva el más alto.

function sum(numberOne, numberTwo) {
    if (numberOne > numberTwo) {
        return ` el numero mayor de los dos es el ${numberOne}`

    } else {
        console.log(` el numero mayor de los dos es el ${numberTwo}`)

    }
}
console.log(sum(7, 3))

console.log(sum(2, 8))

//   **Iteración #2: Buscar la palabra más larga**

//   Completa la función que tomando un array de strings
//    como argumento devuelva el más largo, en caso de que dos
//     strings tenga la misma longitud deberá devolver el primero.

//   Puedes usar este array para probar tu función:
const avengers = ['Hulk', 'Thor', 'IronMan', 'Captain A.', 'Spiderman', 'Captain M.'];
let word = ''
let wordLongest = 0

function findLongestWord() {

    for (let i = 0; i < avengers.length; i++) {
        const heroe = avengers[i];
        if (wordLongest < heroe.length) {
            wordLongest = heroe.length
            word = heroe
        }
    }
    return (word, wordLongest)
}
findLongestWord()
console.log(`la primera palabra que contiene mas cararcteres es ${word}  y tiene ${wordLongest} caracteres`);



// **Iteración #3: Calcular la suma**

// Calcular una suma puede ser tan simple 
// como iterar sobre un array y sumar cada uno de los elementos.
// Implemente la función denominada sumNumbers que toma un array 
// de números como argumento y devuelve la suma de todos los números de la matriz
const numbers = [1, 2, 3, 5, 45, 37, 58];
let suma = 0

function sumAll(param) {
    for (let j = 0; j < numbers.length; j++) {
        const item = numbers[j];
        suma += item
    }
}
sumAll()
console.log(suma);



// **Iteración #4: Calcular el promedio**

// Calcular un promedio es una tarea extremadamente común.
//  Puedes usar este array para probar tu función:

let su = 0
const numberss = [12, 21, 38, 5, 45, 37, 6];

function average(param) {
    for (let a = 0; a < numberss.length; a++) {
        const num = numberss[a];
        su += num
    }
}

average()

let media = su / numberss.length
console.log(media)

// **Iteración #5: Calcular promedio de strings**

// Crea una función que reciba por parámetro un array
//  y cuando es un valor number lo sume y de lo contrario
// cuente la longitud del string y lo sume. Puedes usar este array para probar tu función:


const mixedElements = [6, 1, 'Rayo', 1, 'vallecano', '10', 'upgrade', 8, 'hub'];
let sumaLongString = []
let sumaTotalNumeros = []



    for (let h = 0; h < mixedElements.length; h++) {
        
        const elementos = mixedElements[h];
        if(typeof elementos ==='number') {
            sumaTotalNumeros.push(elementos)
        }else{
            
          sumaLongString.push(elementos.length)
        }
    }

let sumaString = 0
let sumaNumeros = 0
sumaLongString.forEach((item)=>{
    sumaString += item
})
sumaTotalNumeros.forEach((j)=>{
    sumaNumeros += j
})
console.log(sumaString);
console.log(sumaNumeros);

// **Iteración #6: Valores únicos**

// Crea una función que reciba por parámetro un array
//  y compruebe si existen elementos duplicados, en caso 
//  que existan los elimina para retornar un array sin
//   los elementos duplicados. Puedes usar este array para probar tu función:


const duplicates = [
  'sushi',
  'pizza',
  'burger',
  'potatoe',
  'pasta',
  'ice-cream',
  'pizza',
  'chicken',
  'onion rings',
  'pasta',
  'soda'
];
const unicos = duplicates.filter((v,i)=>{return duplicates.indexOf(valor) === indice;})
console.log(unicos);


// **Iteración #7: Buscador de nombres**

// Crea una función que reciba por parámetro 
// un array y el valor que desea comprobar que
//  existe dentro de dicho array - comprueba si 
//  existe el elemento, en caso que existan nos
//   devuelve un true y la posición de dicho elemento 
//   y por la contra un false. Puedes usar este array para probar tu función:


const nameFinder = [
    'Peter',
    'Steve',
    'Tony',
    'Natasha',
    'Clint',
    'Logan',
    'Xabier',
    'Bruce',
    'Peggy',
    'Jessica',
    'Marc'
  ];
  
  /* const finderName = name => nameFinder.includes(name) */
  function finderName(name) {
      
      if(nameFinder.includes(name)===true){return  `el nombre de ${name} si existe en la lista y esta en el puesto ${nameFinder.indexOf(name)}` }
      else if (nameFinder.includes(name)===false){return `el nombre de ${name} no existe en la lista`}
  }
  
  const call1 = finderName('Clint')
  console.log(call1)
  const call = finderName('sergio')
  console.log(call);//devolvera false

  // **Iteration #8: Contador de repeticiones**

// Crea una función que nos devuelva el número 
// de veces que se repite cada una de las palabras 
// que lo conforma.  Puedes usar este array para probar tu función:


const counterWords = [
    'code',
    'repeat',
    'eat',
    'sleep',
    'code',
    'enjoy',
    'sleep',
    'code',
    'enjoy',
    'upgrade',
    'code'
  ];
  
  function repeatCounter(word) {
    let contador = 0
   
  for (let w = 0; w < counterWords.length; w++) {
    
    if(word === counterWords[w]){contador++}
  }
    return contador
  }
  console.log(repeatCounter('code'))