// 1.1 Añade un botón a tu html con el id btnToClick y en tu javascript añade el 
// evento click que ejecute un console log con la información del evento del click

const clicks = document.getElementById('btnToClick')

const ejecutar = ()=>{
    console.log('me has pulsado')
}
clicks.addEventListener('click',ejecutar)

// 1.2 Añade un evento 'focus' que ejecute un console.log con el valor del input.

const focuss = document.querySelector('.focus')

const ejecuta = ()=>{
    console.log(focuss.value)
}
focuss.addEventListener('focus',ejecuta)

// 1.3 Añade un evento 'input' que ejecute un console.log con el valor del input

 const input = document.querySelector('.value')
 const ejecutas = ()=>{
    console.log(input.value)
}
input.addEventListener('input',ejecutas)