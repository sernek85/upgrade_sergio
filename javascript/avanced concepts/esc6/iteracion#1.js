// **Iteración #1: Arrows**


// Crea una arrow function que tenga dos parametros a y b y 
// que por defecto el valor de a = 10 y de b = 5. Haz que la función muestre 
// por consola la suma de los dos parametros.

// 1.1 Ejecuta esta función sin pasar ningún parametro
const arrow = (a=10,b=5)=> a+b

/* let params = arrow() */
console.log(arrow())
// 1.2 Ejecuta esta función pasando un solo parametro
let paramsOne = arrow(4)
console.log(paramsOne);
// 1.3 Ejecuta esta función pasando dos parametros
let paramsTwo = arrow(4,8)
console.log(paramsTwo)