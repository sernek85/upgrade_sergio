// 1.1 Basandote en el array siguiente, crea una lista ul > li 
// dinámicamente en el html que imprima cada uno de los paises.


const countries = ['Japón', 'Nicaragua', 'Suiza', 'Australia', 'Venezuela'];

const ul = document.createElement('ul')
document.body.appendChild(ul)

for (let a = 0; a < countries.length; a++) {
    const li = document.createElement('li')
    li.innerHTML = countries[a]
    ul.appendChild(li)	
}


// 1.2 Elimina el elemento que tenga la clase .fn-remove-me.
const eliminar = document.querySelector('.fn-remove-me')
eliminar.remove()

// 1.3 Utiliza el array para crear dinamicamente una lista ul > li de elementos 
// en el div de html con el atributo data-function="printHere".

 const cars = ['Mazda 6', 'Ford fiesta', 'Audi A4', 'Toyota corola']
const divData = document.querySelector('[data-function]')
 const list = document.createElement('ul')
divData.appendChild(list)
 
 for (let a = 0; a < cars.length; a++) {
     const coche = document.createElement('li')
     coche.innerHTML = cars[a]
     list.appendChild(coche)	
 }
    
    


// 1.4 Crea dinamicamente en el html una lista de div que contenga un elemento 
// h4 para el titulo y otro elemento img para la imagen.

// 1.6 Basandote en el ejercicio anterior. Crea un botón para cada uno de los 
// elementos de las listas que elimine ese mismo elemento del html.
 const countriess = [
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=1'}, 
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=2'},
    {title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=3'},
 	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=4'},
 	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=5'}
 ];

 const divFotos = document.createElement('div')
 document.body.appendChild(divFotos)
 for (let q = 0; q < countriess.length; q++) {
     const foto = countriess[q];
     const h4 = document.createElement('h4')
     const img = document.createElement('img')
     const botonPersonal = document.createElement('button')
     botonPersonal.innerHTML = 'eliminame'
     
     img.src = foto.imgUrl
     h4.innerHTML= foto.title
     h4.appendChild(img)
     divFotos.appendChild(h4)
     h4.appendChild(botonPersonal)
     botonPersonal.addEventListener('click',()=>{
         const bloque = document.querySelector('h4')
        bloque.remove()
     })
     
 }

// 1.5 Basandote en el ejercicio anterior. Crea un botón que elimine el último 
// elemento de la lista.
const boton = document.createElement('button')
boton.innerHTML = 'Eliminar cada uno empezando por el primero'
document.body.appendChild(boton)
boton.addEventListener('click',()=>{
    const bloque = document.querySelector('h4')
    if(countriess.indexOf()){bloque.remove()}
})





